package actions;

import clocks.GameClock;
import game.map.Map;
import game.Mode;
import game.snake.Snake;
import game.powerup.PowerUpManager;
import gui.Gui;
import gui.MenuBar;
import gui.MapLoader;
import security.Crypto;

import java.io.IOException;

public class Main {

    private static final String[] mapfiles = {"map1.png","map2.png","map3.png","map4.png"};
    public static Map[] maps;

    private static final String[] mapnames = {"Map1","Map2","Map3","Map4"};

    public static MapLoader maploader;

    public static int index = 0;

    public static Mode mode;

    public static PowerUpManager powerUpManager;

    public static Map currentMap;

    public static int mapSize = 31;

    public static void nextMap() {
        if(index < mapfiles.length-1){
            index++;
            changemap(index);
        }
    }

    public static void changemap(Map map) {

        currentMap = map;
        updateScore();
    }

    public static void changemap(int i) {

        if (mode == Mode.ENDLESS) return;

        changemap(maps[i]);
    }

    public static void changeMode(Mode _mode, boolean resetScore) {

        mode = _mode;

        if (mode == Mode.ENDLESS) {
            newRandomMap();
        }

        for (Map map : maps) {

            map.setScoreFile(mode);
        }

        if (resetScore) {
            updateScore();
        }
    }

    public static void updateScore() {

        Snake.bestscore = currentMap.highscore;
        Snake.resetSnake(true);
    }

    public static void newRandomMap() {

        currentMap = new Map();
     }

    public static void createMaps() throws IOException {

        maploader = new MapLoader();
        maps = new Map[mapfiles.length];

        for (int i = 0; i < mapfiles.length; i++) {

            maps[i] = new Map(mapfiles[i], mapnames[i]);
        }

        currentMap = maps[0];
    }

    public static void main(String[] args) throws IOException{

        mode = Mode.CLASSIC;
        createMaps();

        Snake.setUp();
        powerUpManager = new PowerUpManager();

        Snake.bestscore = currentMap.highscore;

        Crypto.setProvider();

        Gui g = new Gui();

        GameClock gc = new GameClock();

        g.create();
        gc.start();

    }
}
