package actions;

import game.Mode;
import game.snake.Snake;
import game.powerup.PowerUp;
import gui.Draw;

public class Collision {

    public static boolean invincible = false;

    public static int multi = 1;


    public static boolean collideSelf(){
        for(int i = 0; i <Snake.tails.size(); i++){
            if(     Snake.head.getX() == Snake.tails.get(i).getX()
                    && Snake.head.getY() == Snake.tails.get(i).getY()
                    && !Snake.tails.get(i).isWait()){
                return true;
            }
        }
        return false;
    }

    public static boolean collideWall(){
        if(!invincible) {
            if (Snake.head.getX() < 0 || Snake.head.getX() > Main.currentMap.height-1 || Snake.head.getY() < 0 || Snake.head.getY() > Main.currentMap.width-1)
                return true;

            return Main.currentMap.map[Snake.head.getX()][Snake.head.getY()];
        }

        return false;
    }

    public static void collidePowerup() {
        if(Main.mode == Mode.ARCADE || Main.mode == Mode.ENDLESS){
            for (PowerUp powerUp : Main.powerUpManager.powerUps) {

                if(Snake.head.getX() == powerUp.getX() && Snake.head.getY() == powerUp.getY()) {

                    powerUp.collect();
                }
            }
        }
    }

    public static  void collidePickUp() {
        if(Snake.head.getX() == Snake.pickUp.getX() && Snake.head.getY() == Snake.pickUp.getY()){
            Snake.pickUp.reset();
            Snake.addTail();
            if(multi == 2){
                Snake.addTail();
                Draw.snakecolor = Draw.colorchange(Draw.snakecolor);
            }

            //Score
            Snake.score += multi;

            Draw.snakecolor = Draw.colorchange(Draw.snakecolor);

            if(Snake.score > Snake.bestscore){
                Snake.bestscore = Snake.score;
                Main.currentMap.savescore(Snake.score);
            }
        }
    }
}
