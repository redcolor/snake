package gui;
import actions.KeyHandler;


import javax.swing.*;


public class Gui {

    private JFrame jf;
    private Draw d;

    public MenuBar menuBar;

    static int width = 825, height = 600;
    public static int xoff = 130, yoff = 20;

    public void create(){
       jf = new JFrame( "Snake");
       jf.setSize(width, height);
       jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       jf.setLocationRelativeTo(null);
       jf.setLayout(null);
       jf.setResizable(false);
       jf.addKeyListener(new KeyHandler());

       d = new Draw();
       d.setBounds(0,0,width,height);
       d.setVisible(true);
       jf.add(d);

       jf.requestFocus();


       menuBar = new MenuBar();

       jf.setJMenuBar(menuBar.jMenuBar);
       jf.setVisible(true);
    }
}
