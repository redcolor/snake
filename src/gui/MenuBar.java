package gui;

import actions.Main;
import clocks.GameClock;
import game.Mode;

import javax.swing.*;

public class MenuBar {

    JMenuBar jMenuBar;

    MenuBar() {

        setup();
    }

    private void setup() {

        jMenuBar = new JMenuBar();
        JMenu menu1 = new JMenu("Level");
        jMenuBar.add(menu1);
        JMenu menu2 = new JMenu("Settings");
        jMenuBar.add(menu2);
        JMenu menu3 = new JMenu("Credits");
        jMenuBar.add(menu3);
        JMenu menu4 = new JMenu("Modus");
        jMenuBar.add(menu4);

        //---------------------------------------------------//
        //Credits//
        JMenuItem cred = new JMenuItem("Credits");
        menu3.add(cred);

        cred.addActionListener(actionEvent -> {

            Credits c = new Credits();
            c.create();
        });


        //---------------------------------------------------//
        //Modi

        JMenuItem classic = new JMenuItem("Classic");
        menu4.add(classic);
        classic.addActionListener(actionEvent ->{

            Main.changeMode(Mode.CLASSIC, true);
        });

        JMenuItem arcade = new JMenuItem("Arcade");
        menu4.add(arcade);
        arcade.addActionListener(actionEvent -> {

            Main.changeMode(Mode.ARCADE, true);
        });

        JMenuItem endless = new JMenuItem("Endless");
        menu4.add(endless);
        endless.addActionListener(actionEvent -> {

            Main.changeMode(Mode.ENDLESS, true);
        });


        //---------------------------------------------------//
        //Pause//
        JMenuItem item = new JMenuItem("Pause  : P");
        menu2.add(item);

        item.addActionListener(e -> {
            if(GameClock.running){
                GameClock.running = false;
            }
            else{
                GameClock.running = true;
                GameClock gc = new GameClock();
                gc.start();
            }
        });
        //---------------------------------------------------//
        //Map MenuItems//

        JMenuItem[] levels = new JMenuItem[Main.maps.length];

        for (int i = 0; i < levels.length; i++) {

            levels[i] = new JMenuItem("Level " + (i + 1) + (i == 0 ? "" : " Score_Level " + i + " (15)"));
            menu1.add(levels[i]);

            int index = i;

            levels[i].addActionListener(actionEvent -> {

                if(index == 0 || Main.maps[index - 1].highscore > 14) {

                    Main.changemap(index);
                }
            });
        }
    }
}
