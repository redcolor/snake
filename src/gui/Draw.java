package gui;

import actions.Main;
import game.Mode;
import game.snake.Snake;
import game.powerup.PowerUp;

import javax.swing.*;
import java.awt.*;

public class Draw extends JLabel {

    public static Color snakecolor = new Color(0, 255, 0);
    public static final Color startcolor = new Color(0, 255, 0);

    static boolean blaufull = false;
    static boolean rotfull = false;
    static boolean gruenfull = true;

    Point p;

    private long start;
    private long end;

    private final double maxFrameTime = (1000.0 / 60.0);

    protected void paintComponent(Graphics g){

        start = System.currentTimeMillis();

        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);

        //Draw Background
        g.setColor(Color.LIGHT_GRAY);
        g.fillRect(0, 0, Gui.width, Gui.height);

        //Draw Grid
        g.setColor(Color.BLACK);
        for(int i = 0; i < Main.currentMap.height; i++){
            for (int j = 0; j < Main.currentMap.width; j++){
                if(!Main.currentMap.map[i][j]) {
                    g.drawRect(i * Main.currentMap.height/2 + Gui.xoff, j * Main.currentMap.height/2 + Gui.yoff, Main.currentMap.height/2, Main.currentMap.height/2);
                }
                if(Main.currentMap.map[i][j]){
                    g.fillRect(i * Main.currentMap.height/2 + Gui.xoff, j * Main.currentMap.height/2 + Gui.yoff, Main.currentMap.height/2, Main.currentMap.height/2);
                }
            }
        }

        //Draw Snake Tails
        g.setColor(snakecolor);
        for (int i = 0; i< Snake.tails.size(); i++){
            p = Snake.ptc(Snake.tails.get(i).getX(),Snake.tails.get(i).getY());
            g.fillRect(p.x,p.y,Main.currentMap.height/2,Main.currentMap.height/2);
        }

        //Draw Head
        g.setColor(getHeadColor());
        p = Snake.ptc(Snake.head.getX(), Snake.head.getY());
        g.fillRect(p.x,p.y,Main.currentMap.height/2,Main.currentMap.height/2);

        //Draw PickUP
        g.setColor(new Color(255, 144, 0));
        p = Snake.ptc(Snake.pickUp.getX(),Snake.pickUp.getY());
        g.fillRect(p.x,p.y,Main.currentMap.height/2,Main.currentMap.height/2);


        //Draw Powerups
        if(Main.mode == Mode.ARCADE || Main.mode == Mode.ENDLESS){

            //Draw Timer
            g.setColor(Color.BLACK);
            g.setFont(new Font("Roboto", Font.BOLD, 20));
            g.drawString("PowerUp Timer",650,75);
            g.drawString("Fortitude: " + Main.powerUpManager.powerUps[0].getTimeLeft(),650,100);
            g.drawString("DoubleScore: " + Main.powerUpManager.powerUps[1].getTimeLeft() ,650,125);
            g.drawString("Timeshift: " + Main.powerUpManager.powerUps[2].getTimeLeft(),650,150);

            //Draw Color Timer
            int anfang = 87;
            for (PowerUp powerUp : Main.powerUpManager.powerUps) {
                g.setColor(powerUp.color);

                g.fillRect(800,anfang,Main.currentMap.height/2,Main.currentMap.height/2);
                anfang += 25;
            }


            for (PowerUp powerUp : Main.powerUpManager.powerUps) {
                g.setColor(powerUp.color);
                p = Snake.ptc(powerUp.getX(), powerUp.getY());
                g.fillRect(p.x,p.y,Main.currentMap.height/2,Main.currentMap.height/2);
            }
        }

        //Draw Border
        g.setColor(Color.DARK_GRAY);
        g.drawRect(Gui.xoff, Gui.yoff, 512,512);

        //Draw Score
        g.setColor(Color.BLACK);
        g.setFont(new Font("Roboto", Font.BOLD, 20));
        g.drawString("Score:" + Snake.score,5,25);
        g.drawString("Best:" + Snake.bestscore,650,25);
        g.drawString("Last:" + Snake.lastscore, 5,50);

        end = System.currentTimeMillis();

        long waitTime = (long) Math.max(0, maxFrameTime - (end - start));

        try {
            Thread.sleep(waitTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // System.out.println(1000.0 / (System.currentTimeMillis() - start) + " fps");      // Log fps
        repaint();
    }

    public static Color colorchange(Color color){
        int add = 15;
        int sub = 15;

        int rot = color.getRed();
        int blau = color.getBlue();
        int grün = color.getGreen();

        if(grün > 0 && gruenfull){
            grün -= sub;
            blau += add;

            if(blau >= 255){
                grün = 0;
                blau = 255;
                blaufull = true;
                gruenfull = false;
            }
        }
        if(blau > 0 && blaufull){
            blau -= sub;
            rot += add;

            if(rot >= 255){
                blau = 0;
                rot = 255;
                rotfull = true;
                blaufull = false;
            }
        }
        if(rot > 0 && rotfull){
            grün += add;
            rot -= sub;

            if(grün >= 255){
                rot = 0;
                grün = 255;
                rotfull = false;
                gruenfull = true;
            }
        }
        return new Color(rot, grün,blau);
    }

    public static Color getHeadColor(){
        float teiler = 1.5f;
        int rot = (int) (snakecolor.getRed() / teiler);
        int blau = (int) (snakecolor.getBlue() / teiler);
        int grün = (int) (snakecolor.getGreen() / teiler);

        if(rot < 0){
            rot = 0;
        }
        if(blau < 0){
            blau = 255;
        }
        if(grün < 0){
            grün = 0;
        }

        return new Color(rot, grün,blau);
    }
}
