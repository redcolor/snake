package gui;

import javax.swing.*;

public class Credits {

    public JFrame creditframe;

    public void create() {

        JPanel creditList = new JPanel();

        creditframe = new JFrame("Credits");
        creditframe.setSize(500, 250);
        creditframe.setLocationRelativeTo(null);
        creditframe.setResizable(false);

        creditList.setLayout(new BoxLayout(creditList, BoxLayout.PAGE_AXIS));

        creditList.add(new JLabel("       "));
        creditList.add(new JLabel("        Credits:"));
        creditList.add(new JLabel("                Game Director & Lead Programmer"));
        creditList.add(new JLabel("                    Christian Heidecke"));
        creditList.add(new JLabel("                Co-Programmer & Credit Fixer"));
        creditList.add(new JLabel("                    Henrik Huth"));
        creditList.add(new JLabel("                Map Design"));
        creditList.add(new JLabel("                    Jannek Pufe"));
        creditList.add(new JLabel("                Debugger & Game Tester"));
        creditList.add(new JLabel("                    Tristan Mackay"));
        creditList.add(new JLabel("                Map Design & Debugger"));
        creditList.add(new JLabel("                    Bennet Biastoch"));


        creditframe.add(creditList);
        creditframe.requestFocus();
        creditframe.setVisible(true);
    }
}
