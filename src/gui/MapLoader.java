package gui;

import game.map.MapData;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class MapLoader {

    public MapData loadMap(String file) throws IOException {

        BufferedImage img = ImageIO.read(new File(file));

        boolean[][] map = new boolean[img.getHeight()][img.getWidth()];

        int height = img.getHeight();
        int width = img.getWidth();

        for (int x = 0; x < img.getHeight() ; x++) {
            for (int y = 0; y < img.getWidth(); y++) {

                map[x][y] = new Color(img.getRGB(x,y)).getRed() > 0;
            }
        }

        return new MapData(map, width, height);
    }

}
