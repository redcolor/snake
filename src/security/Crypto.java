package security;

import java.io.*;
import javax.crypto.*;
import javax.crypto.spec.*;

public class Crypto {

    public static void setProvider() {
        java.security.Security.addProvider(new com.sun.crypto.provider.SunJCE());
    }

    public static void encryptFile(String bestscorestring, String encryptedFile, String password) throws Exception {
        CipherOutputStream out;

        Cipher cipher;
        SecretKey key;
        cipher = Cipher.getInstance("DES");
        key = new SecretKeySpec(password.getBytes(), "DES");
        cipher.init(Cipher.ENCRYPT_MODE, key);

        out = new CipherOutputStream(new FileOutputStream(encryptedFile), cipher);

        out.write(bestscorestring.getBytes());
        out.close();
    }


    public static String decryptFile(String encryptedFile, String password) throws Exception {
        CipherInputStream in;
        String bestscorestring;
        Cipher cipher;
        SecretKey key;
        cipher = Cipher.getInstance("DES");
        key = new SecretKeySpec(password.getBytes(), "DES");
        cipher.init(Cipher.DECRYPT_MODE, key);
        in = new CipherInputStream(new FileInputStream(encryptedFile), cipher);
        bestscorestring = new String(in.readAllBytes());

        in.close();

        return bestscorestring;
    }
}
