package clocks;

import actions.Collision;
import actions.Main;
import game.Mode;
import game.snake.Snake;
import gui.Draw;

public class GameClock extends Thread{
    public static boolean running = true;

    public static int frametime = 150;
    public  final static int defaultframetime = 150;

    public void run(){
        while(running){
            try {

                sleep(frametime);
                Snake.move();
                Snake.waitToMove = false;

                Main.powerUpManager.update();

                Collision.collidePickUp();
                Collision.collidePowerup();


                if(Collision.collideSelf()){
                    if(Main.mode == Mode.ENDLESS){
                        Main.newRandomMap();
                    }
                    Snake.tails.clear();
                    //Score
                    Snake.score = 0;

                    Draw.snakecolor = Draw.startcolor;
                }
                if(Collision.collideWall()){
                    if(Main.mode == Mode.ENDLESS){
                        Main.newRandomMap();
                    }
                    Snake.resetSnake(false);
                }

            } catch (Exception e){
                e.printStackTrace();
            }

        }
    }
}
