package game.powerup;

import actions.Collision;
import actions.Main;
import clocks.GameClock;

import java.awt.*;
import java.util.concurrent.ThreadLocalRandom;

public class PowerUp {

    public final PowerUpType type;
    public Color color;

    private long stopTime = Long.MAX_VALUE;

    float timeLeft;

    int x,y;


    public PowerUp(PowerUpType type) {

        this.type = type;

        switch(type) {

            case FORTITUDE:
                color = new Color(37, 45, 255);
                break;
            case DOUBLESCORE:
                color = new Color(255, 88, 111);
                break;
            case TIMESHIFT:
                color = new Color(99, 255, 218);
                break;
        }
    }

    public void collect() {

        Main.powerUpManager.resetPowerUp(this);

        //isActive = true;



        switch (type) {

            case FORTITUDE:
                Collision.invincible = true;
                stopTime = System.currentTimeMillis() + 5000;
                break;
            case TIMESHIFT:
                stopTime = System.currentTimeMillis() + 10000;
                if(Math.random() < 0.5){
                    GameClock.frametime = 200;
                }
                else{
                    GameClock.frametime = 100;
                }
                break;
            case DOUBLESCORE:
                Collision.multi = 2;
                stopTime = System.currentTimeMillis() + 20000;
                break;
        }
    }

    public void update() {

        if(stopTime == Long.MAX_VALUE) {
            timeLeft = 0;
        } else {
            timeLeft = stopTime - System.currentTimeMillis();
            timeLeft /= 1000;
        }

        if (stopTime >= System.currentTimeMillis()) {
            return;
        }

        reset();
    }

    public void reset() {

        stopTime = Long.MAX_VALUE;

        //isActive = false;

        switch (type) {

            case FORTITUDE:
                Collision.invincible = false;
                break;
            case TIMESHIFT:

                GameClock.frametime = GameClock.defaultframetime;
                break;
            case DOUBLESCORE:
                Collision.multi = 1;
                break;


        }

    }

    public String getTimeLeft() {

        if(timeLeft <= 0) return "";
        return (int)timeLeft + "";
    }

    public void newPosition(){

        this.x = ThreadLocalRandom.current().nextInt(0,Main.mapSize);
        this.y = ThreadLocalRandom.current().nextInt(0,Main.mapSize);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
