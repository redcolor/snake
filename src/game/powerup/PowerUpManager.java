package game.powerup;

import actions.Main;
import game.snake.Snake;

public class PowerUpManager {

    public PowerUp[] powerUps;

    public boolean[][] powerUpsPos;

    public PowerUpManager() {

        powerUps = new PowerUp[PowerUpType.values().length];

        for (int i = 0; i <= powerUps.length - 1; i++) {

            powerUps[i] = new PowerUp(PowerUpType.values()[i]);
        }

        powerUpsPos = new boolean[Main.currentMap.height][Main.currentMap.width];

        for (int i = 0; i < powerUps.length; i++) {

            powerUps[i].newPosition();

            int x = powerUps[i].getX();
            int y = powerUps[i].getY();

            if (Main.currentMap.map[x][y]
                || (Snake.pickUp.getX() == x && Snake.pickUp.getY() == y)
                || powerUpsPos[x][y]) {

                i--;
                continue;
            }

            powerUpsPos[x][y] = true;
        }
    }

    public void resetAll() {

        for (PowerUp powerUp : powerUps) {
            resetPowerUp(powerUp);
            powerUp.reset();
        }
    }

    public void resetPowerUp(PowerUp powerUp) {
        powerUp.newPosition();

        int x = powerUp.getX();
        int y = powerUp.getY();

        powerUpsPos[x][y] = false;

        if (Main.currentMap.map[x][y]
                || (Snake.pickUp.getX() == x && Snake.pickUp.getY() == y)
                || powerUpsPos[x][y]) {

            resetPowerUp(powerUp);
        }
    }

    public void update() {

        for (int i = 0; i <= powerUps.length - 1; i++) {

            powerUps[i].update();
        }
    }
}
