package game.powerup;

public enum PowerUpType {

    FORTITUDE, DOUBLESCORE, TIMESHIFT
}
