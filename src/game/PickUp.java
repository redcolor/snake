package game;

import actions.Main;

import java.util.concurrent.ThreadLocalRandom;

public class PickUp {
    public int x,y;

    public PickUp(){
         reset();
    }

    public void reset(){

        this.x = ThreadLocalRandom.current().nextInt(0,Main.mapSize);
        this.y = ThreadLocalRandom.current().nextInt(0,Main.mapSize);

        if (Main.currentMap.map[this.x][this.y]) reset();
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
