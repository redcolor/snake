package game.snake;

import actions.Main;
import game.PickUp;
import gui.Draw;
import gui.Gui;

import java.awt.*;
import java.util.ArrayList;

public class Snake {

    public static int score = 0;
    public static int lastscore = 0;

    public static int bestscore = 0;

    public static boolean waitToMove = false;
    public static Head head = new Head(16,16);
    public static ArrayList<Tail> tails;

    public static PickUp pickUp;

    public static void setUp() {
        pickUp = new PickUp();
        tails = new ArrayList<>();
    }

    public static void addTail(){
        if(tails.size() < 1 ){
            tails.add(new Tail(head.getX(), head.getY()));

        }else{
            tails.add(new Tail(tails.get(tails.size()-1).x,tails.get(tails.size()-1).y));
        }
    }

    public static void move(){
        //Tail Move
        if(tails.size() >= 2){
            for (int i = tails.size()-1; i >= 1; i--){
                if (tails.get(i).isWait()){
                    tails.get(i).setWait(false);
                }
                else{
                    tails.get(i).setX(tails.get(i-1).getX());
                    tails.get(i).setY(tails.get(i-1).getY());
                }
            }
        }
        //Move First Tail
        if(tails.size()  >= 1){
            if (tails.get(0).isWait()){
                tails.get(0).setWait(false);
            }
            else{
                tails.get(0).setX(head.getX());
                tails.get(0).setY(head.getY());
            }
        }
        //Move Head
        switch (head.getDir()){
            case RIGHT:
                head.setX(head.getX()+1);
                break;
            case LEFT:
                head.setX(head.getX()-1);
                break;
            case DOWN:
                head.setY(head.getY()+1);
                break;
            case UP:
                head.setY(head.getY()-1);
                break;
        }

    }
    //Position zu Koordinaten
    public static Point ptc(int x,int y){
        Point p = new Point(0,0);
        p.x = x*16 + Gui.xoff;
        p.y = y*16 + Gui.yoff;

        return p;
    }

    public static void resetSnake(boolean change_map){
        Snake.tails.clear();
        Snake.head.setX(16);
        Snake.head.setY(16);
        pickUp.reset();
        Main.powerUpManager.resetAll();

        if(change_map) {
            Snake.lastscore = 0;
        }else if(Snake.score > 0) {
            Snake.lastscore = Snake.score;
        }
        Snake.score = 0;

        Draw.snakecolor = Draw.startcolor;
    }
}
