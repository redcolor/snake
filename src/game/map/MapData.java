package game.map;

public class MapData {

    public final int width;
    public final int height;
    public boolean[][] map;

    public MapData( boolean[][] map, int width, int height) {
        this.width = width;
        this.height = height;
        this.map = map;
    }
}
