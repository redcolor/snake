package game.map;

import actions.Main;
import game.Mode;
import security.Crypto;

import java.io.File;
import java.io.IOException;

public class Map {

    public final String name;

    public File scoreFile;

    public int highscore = -1;

    public boolean[][] map;
    public int height;
    public int width;

    public Map() {

        height = 32;
        width = 32;

        name = "random";
        setScoreFile(Main.mode);

        if (!scoreFile.exists()) {
            try {
                if (!scoreFile.createNewFile()) throw new IOException("Score File could not be created.");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        map = NoiseGenerator.generateBoolMap(32, 32, 5f, 0.7f);
    }

    public Map(String file, String name) throws IOException {

        this.name = name;

        setScoreFile(Main.mode);

        MapData data = Main.maploader.loadMap(file);

        this.map = data.map;
        this.height = data.height;
        this.width = data.width;

        loadscore();
    }

    public void setScoreFile(Mode mode) {

        switch (mode) {
            case CLASSIC:
                scoreFile = new File("score_" + name);
                break;
            case ARCADE:
                scoreFile = new File("score_" + name + mode.toString());
                break;
            case ENDLESS:
                scoreFile = new File("score_endless");
                break;
        }

        if (!scoreFile.exists()){
            try {
                if (!scoreFile.createNewFile()) throw new IOException("Score File could not be created.");
            } catch (IOException e) {
                System.err.println("Error creating score file " + scoreFile.getName());
                e.printStackTrace();
            }
        }
        loadscore();
    }


    private void loadscore(){

        try {
            highscore = Integer.parseInt(Crypto.decryptFile(scoreFile.getName(), "12345678"));
        } catch (NumberFormatException e) {
            highscore = 0;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void savescore(int score){

        highscore = score;

        try {
            Crypto.encryptFile(score +"", "score_" + name, "12345678");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
