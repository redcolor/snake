package game.map;

import static game.map.SimplexNoise.noise;

public class NoiseGenerator {

    private static float[][] generateSimplexNoise(int width, int height, float frequency){

        float[][] simplexnoise = new float[width][height];
        float _frequency = frequency / (float) width;

        double deltaX = Math.random() * 10000;
        double deltaY = Math.random() * 10000;

        for(int x = 0; x < width; x++){
            for(int y = 0; y < height; y++){
                simplexnoise[x][y] = (float) noise(x * _frequency + deltaX, y * _frequency + deltaY);
                simplexnoise[x][y] = (simplexnoise[x][y] + 1) / 2;   //generate values between 0 and 1
            }
        }

        return simplexnoise;
    }

    public static boolean[][] generateBoolMap(int width, int height, float frequency, float threshold) {

        if (threshold < 0 || threshold > 1) throw new IllegalArgumentException("Threshold must be between 0 and 1");

        float[][] simplexnoise = generateSimplexNoise(width, height, frequency);

        boolean[][] rdmboolmap = new boolean[width][height];
        for (int y = 0; y < width; y++) {
            for (int x = 0; x < height; x++) {
                rdmboolmap[y][x] = simplexnoise[y][x] > threshold;

                if(width/2 == y && height/2 == x){
                    rdmboolmap[y][x] =  false;
                }
            }
        }
        return rdmboolmap;
    }
}
